<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\User;
use App\Entity\Rating;
use App\Form\GameType;
use App\Form\RatingType;
use App\Repository\GameRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[Route('/game')]
class GameController extends AbstractController
{
    #[Route('/', name: 'game_index', methods: ['GET'])]
    public function index(GameRepository $gameRepository): Response
    {
        return $this->render('game/index.html.twig', [
            'games' => $gameRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'game_new', methods: ['GET','POST'])]
    #[IsGranted('ROLE_ADMIN')]

    public function new(Request $request): Response
    {
        $game = new Game();
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $game->setCreateDate(new \DateTime('now'));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($game);
            $entityManager->flush();

            return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/new.html.twig', [
            'game' => $game,
            'form' => $form,
        ]);
    }

    #[Route('/favorites/add/{id}', name: 'add_favorites')]
    public function addFavorites(Game $game)
    {
        if(!$game) {
            throw new NotFoundHttpException('Pas de jeu trouvé');
        }

        $game->addUser($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($game);
        $em->flush();

        return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/favorites/remove/{id}', name: 'remove_favorites')]
    public function removeFavorites(Game $game)
    {
        if(!$game) {
            throw new NotFoundHttpException('Pas de jeu trouvé');
        }

        $game->removeUser($this->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($game);
        $em->flush();

        return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}', name: 'game_show', methods: ['GET','POST'])]
    public function show(Game $game, Request $request): Response
    {   
        $user = $this->getUser();
        $comment = new Rating();
        $form = $this->createForm(RatingType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setStatus(0);
            $comment->setCreatedAt(new \DateTime('now'));
            $comment->setGame($game);
            $comment->setUser($user);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();
        }

        return $this->render('game/show.html.twig', [
            'game' => $game,
            'ratingComment' => $form->createView(), 
            
        ]);
    }


    #[Route('/{id}/edit', name: 'game_edit', methods: ['GET','POST'])]
    #[IsGranted('ROLE_ADMIN')]

    public function edit(Request $request, Game $game): Response
    {
        $form = $this->createForm(GameType::class, $game);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('game/edit.html.twig', [
            'game' => $game,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'game_delete', methods: ['POST'])]
    public function delete(Request $request, Game $game): Response
    {
        if ($this->isCsrfTokenValid('delete'.$game->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($game);
            $entityManager->flush();
        }

        return $this->redirectToRoute('game_index', [], Response::HTTP_SEE_OTHER);
    }
}
