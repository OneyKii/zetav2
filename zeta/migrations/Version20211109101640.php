<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109101640 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE game DROP FOREIGN KEY FK_232B318CE9D22C39');
        $this->addSql('ALTER TABLE game_genre_genre DROP FOREIGN KEY FK_E0036E3FE9D22C39');
        $this->addSql('DROP TABLE game_genre');
        $this->addSql('DROP TABLE game_genre_genre');
        $this->addSql('DROP INDEX IDX_232B318CE9D22C39 ON game');
        $this->addSql('ALTER TABLE game DROP game_genre_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE game_genre (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE game_genre_genre (game_genre_id INT NOT NULL, genre_id INT NOT NULL, INDEX IDX_E0036E3FE9D22C39 (game_genre_id), INDEX IDX_E0036E3F4296D31F (genre_id), PRIMARY KEY(game_genre_id, genre_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE game_genre_genre ADD CONSTRAINT FK_E0036E3F4296D31F FOREIGN KEY (genre_id) REFERENCES genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game_genre_genre ADD CONSTRAINT FK_E0036E3FE9D22C39 FOREIGN KEY (game_genre_id) REFERENCES game_genre (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE game ADD game_genre_id INT NOT NULL');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318CE9D22C39 FOREIGN KEY (game_genre_id) REFERENCES game_genre (id)');
        $this->addSql('CREATE INDEX IDX_232B318CE9D22C39 ON game (game_genre_id)');
    }
}
